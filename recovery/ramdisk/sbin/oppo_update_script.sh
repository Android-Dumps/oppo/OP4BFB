#!/sbin/sh
para_list=$@

#Symlink funtion
if [ "$1" = "symlink" ]; then
    shift
    link_src=$1
    shift
    for link_dest in "$@"
    do
        if [ ${link_dest:0:1} != "/" ]; then
            echo "link dest path not start with root dir"
            exit 1
        fi

        link_dir=$(dirname ${link_dest})
        rm -rf link_dest
        cd ${link_dir}
        ln -sf ${link_src} ${link_dest}
    done
    exit 0
fi

if [ "$1" = "system_user" ]; then
    user_source=/data/misc/vold/user_keys/test_secret
    user_target=/data/misc/test_secret
    if [ -e $user_source ];then
        mount /dev/block/platform/bootdevice/by-name/system /system_root
        mv $user_source $user_target
        chmod 0777 $user_target
        chown 1000:1000 $user_target
        chcon u:object_r:system_data_file:s0 $user_target
        umount /system_root
    fi
    exit 0
fi

if [ "$1" = "migrate_font" ]; then
    font_source=/data/system/font/ColorOS-Regular.ttf
    font_target=/data/format_unclear/font/ColorOS-Regular.ttf
    if [ -e $font_source ];then
        mv $font_source $font_target
        chmod 0777 $font_target
        chown 1000:1000 $font_target
    fi
    exit 0
fi

if [ "$1" = "fpdata_migrate" ]; then
    #migrate finger data
    fpdata_o="/data/system/users/0/fpdata"
    fpdata_path="/data/vendor_de/0"
    fpdata_p="/data/vendor_de/0/fpdata"
    if [ ! -d $fpdata_p ] && [ -d $fpdata_o ];then
        mkdir -p $fpdata_path
        mv $fpdata_o $fpdata_path
        chmod -R 700 $fpdata_p
        chown -R 1000:1000 $fpdata_p
        #make sure /system mounted if need chcon
        chcon -R u:object_r:fingerprint_vendor_data_file:s0 $fpdata_p
        rm -rf $fpdata_o
        echo "fpdata migrate done."
    fi

    #migrate face data
    fcdata_o="/data/system/users/0/facedata"
    fcdata_path="/data/vendor_de/0"
    fcdata_p="/data/vendor_de/0/facedata"
    if [ ! -d $fcdata_p ] && [ -d $fcdata_o ];then
        mkdir -p $fcdata_path
        mv $fcdata_o $fcdata_p
        chmod -R 700 $fcdata_p
        chown -R 1000:1000 $fcdata_p
        #make sure /system mounted if need chcon
        chcon -R u:object_r:face_data_file:s0 $fcdata_p
        rm -rf $fcdata_o
        echo "fcdata migrate done."
    fi

    #migrate cryptoeng data
    cryptoeng_o="/data/system/users/0/cryptoeng"
    cryptoeng_path="/data/vendor_de/0"
    cryptoeng_p="/data/vendor_de/0/cryptoeng"
    if [ ! -d $cryptoeng_p ] && [ -d $cryptoeng_o ];then
        mkdir -p $cryptoeng_path
        mv $cryptoeng_o $cryptoeng_p
        chmod -R 700 $cryptoeng_p
        chown -R 1000:1000 $cryptoeng_p
        #make sure /system mounted if need chcon
        chcon -R u:object_r:cryptoeng_data_file:s0 $cryptoeng_p
        rm -rf $cryptoeng_o
        echo "cryptoeng migrate done."
    fi

    #migrate face data from p to p
    fcdata_p_old="/data/vendor/users/0/facedata"
    fcdata_path_p_new="/data/vendor_de/0"
    fcdata_p_n="/data/vendor_de/0/facedata"
    if [ ! -d $fcdata_p_n ] && [ -d $fcdata_p_old ];then
        mkdir -p $fcdata_path_p_new
        mv $fcdata_p_old $fcdata_p_n
        chmod -R 700 $fcdata_p_n
        chown -R 1000:1000 $fcdata_p_n
        #make sure /system mounted if need chcon
        chcon -R u:object_r:face_data_file:s0 $fcdata_p_n
        rm -rf $fcdata_p_old
        echo "fcdata migrate done from p to p."
    fi

    #migrate cryptoeng data from p to p
    cryptoeng_p_old="/data/vendor/users/0/cryptoeng"
    cryptoeng_path_p_new="/data/vendor_de/0"
    cryptoeng_p_n="/data/vendor_de/0/cryptoeng"
    if [ ! -d $cryptoeng_p_n ] && [ -d $cryptoeng_p_old ];then
        mkdir -p $cryptoeng_path_p_new
        mv $cryptoeng_p_old $cryptoeng_p_n
        chmod -R 700 $cryptoeng_p_n
        chown -R 1000:1000 $cryptoeng_p_n
        #make sure /system mounted if need chcon
        chcon -R u:object_r:cryptoeng_data_file:s0 $cryptoeng_p_n
        rm -rf $cryptoeng_p_old
        echo "cryptoeng migrate done from p to p."
    fi

    #migrate gf_data data
    gf_data_o="/data/system/gf_data"
    gf_data_path="/data/vendor_de/0/fpdata"
    gf_data_p="/data/vendor_de/0/fpdata/gf_data"
    if [ ! -d $gf_data_p ] && [ -d $gf_data_o ];then
        mkdir -p $gf_data_path
        mv $gf_data_o $gf_data_p
        chmod -R 700 $gf_data_p
        chown -R 1000:1000 $gf_data_p
        #make sure /system mounted if need chcon
        chcon -R u:object_r:face_data_file:s0 $gf_data_p
        rm -rf $gf_data_o
        echo "gf_data migrate done."
    fi
fi
#Other fuction

#Other fuction
