#I##########################################################################################################
#  Infinity-Box Chinese Miracle II
#  General Setting
#  Version : 2.18 , (c) Infinity-Box Team, 2020
#I##########################################################################################################
- general: MTK_PLATFORM_CFG
  info: 
    - config_version: V1.1.3
      platform: MT6765
      project: oppo6765_19451
      storage: EMMC
      boot_channel: MSDC_0
      block_size: 0x20000
#I##########################################################################################################
#
#  Layout Setting
#
#I##########################################################################################################

- partition_index: SYS0
  partition_name: preloader
  file_name: preloader_oppo6765_19451.bin
  is_download: true
  type: SV5_BL_BIN
  linear_start_addr: 0x0
  physical_start_addr: 0x0
  partition_size: 0x400000
  region: EMMC_BOOT1_BOOT2
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: BOOTLOADERS
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS1
  partition_name: pgpt
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x0
  physical_start_addr: 0x0
  partition_size: 0x8000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS2
  partition_name: boot_para
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x8000
  physical_start_addr: 0x8000
  partition_size: 0x100000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS3
  partition_name: recovery
  file_name: recovery.img
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x108000
  physical_start_addr: 0x108000
  partition_size: 0x4000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS4
  partition_name: para
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x4108000
  physical_start_addr: 0x4108000
  partition_size: 0x80000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS5
  partition_name: opporeserve1
  file_name: opporeserve.img
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x4188000
  physical_start_addr: 0x4188000
  partition_size: 0x800000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS6
  partition_name: opporeserve2
  file_name: opporeserve.img
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x4988000
  physical_start_addr: 0x4988000
  partition_size: 0x4000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS7
  partition_name: opporeserve3
  file_name: opporeserve3.img
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x8988000
  physical_start_addr: 0x8988000
  partition_size: 0x4000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS8
  partition_name: oppo_custom
  file_name: oppo_custom.img
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0xc988000
  physical_start_addr: 0xc988000
  partition_size: 0x100000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS9
  partition_name: expdb
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0xca88000
  physical_start_addr: 0xca88000
  partition_size: 0x1400000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS10
  partition_name: frp
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0xde88000
  physical_start_addr: 0xde88000
  partition_size: 0x100000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS11
  partition_name: nvcfg
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0xdf88000
  physical_start_addr: 0xdf88000
  partition_size: 0x2000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS12
  partition_name: nvdata
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0xff88000
  physical_start_addr: 0xff88000
  partition_size: 0x4000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS13
  partition_name: metadata
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x13f88000
  physical_start_addr: 0x13f88000
  partition_size: 0x2000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS14
  partition_name: protect1
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x15f88000
  physical_start_addr: 0x15f88000
  partition_size: 0x800000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS15
  partition_name: protect2
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x16788000
  physical_start_addr: 0x16788000
  partition_size: 0x878000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS16
  partition_name: seccfg
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x17000000
  physical_start_addr: 0x17000000
  partition_size: 0x800000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS17
  partition_name: persist
  file_name: persist.img
  is_download: false
  type: EXT4_IMG
  linear_start_addr: 0x17800000
  physical_start_addr: 0x17800000
  partition_size: 0x3000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS18
  partition_name: sec1
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x1a800000
  physical_start_addr: 0x1a800000
  partition_size: 0x200000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS19
  partition_name: proinfo
  file_name: proinfo.img
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x1aa00000
  physical_start_addr: 0x1aa00000
  partition_size: 0x300000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: PROTECTED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS20
  partition_name: md1img
  file_name: md1rom.img
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x1ad00000
  physical_start_addr: 0x1ad00000
  partition_size: 0x6400000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS21
  partition_name: spmfw
  file_name: spmfw.img
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x21100000
  physical_start_addr: 0x21100000
  partition_size: 0x100000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS22
  partition_name: scp1
  file_name: tinysys-scp.bin
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x21200000
  physical_start_addr: 0x21200000
  partition_size: 0x100000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS23
  partition_name: scp2
  file_name: tinysys-scp.bin
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x21300000
  physical_start_addr: 0x21300000
  partition_size: 0x100000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS24
  partition_name: sspm_1
  file_name: tinysys-sspm.bin
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x21400000
  physical_start_addr: 0x21400000
  partition_size: 0x100000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS25
  partition_name: sspm_2
  file_name: tinysys-sspm.bin
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x21500000
  physical_start_addr: 0x21500000
  partition_size: 0x100000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS26
  partition_name: gz1
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x21600000
  physical_start_addr: 0x21600000
  partition_size: 0x1000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS27
  partition_name: gz2
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x22600000
  physical_start_addr: 0x22600000
  partition_size: 0x1000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: INVISIBLE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS28
  partition_name: nvram
  file_name: nvram.bin
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0x23600000
  physical_start_addr: 0x23600000
  partition_size: 0x4000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: BINREGION
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS29
  partition_name: lk
  file_name: lk.bin
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x27600000
  physical_start_addr: 0x27600000
  partition_size: 0x500000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: true
  reserve: 0x00

- partition_index: SYS30
  partition_name: lk2
  file_name: lk.bin
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x27b00000
  physical_start_addr: 0x27b00000
  partition_size: 0x500000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS31
  partition_name: boot
  file_name: boot.img
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x28000000
  physical_start_addr: 0x28000000
  partition_size: 0x2000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS32
  partition_name: logo
  file_name: logo.bin
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x2a000000
  physical_start_addr: 0x2a000000
  partition_size: 0x1000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: true
  reserve: 0x00

- partition_index: SYS33
  partition_name: dtbo
  file_name: dtbo.img
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x2b000000
  physical_start_addr: 0x2b000000
  partition_size: 0x1000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS34
  partition_name: tee1
  file_name: tz.img
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x2c000000
  physical_start_addr: 0x2c000000
  partition_size: 0x500000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: true
  reserve: 0x00

- partition_index: SYS35
  partition_name: tee2
  file_name: tz.img
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x2c500000
  physical_start_addr: 0x2c500000
  partition_size: 0xb00000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS36
  partition_name: odm
  file_name: odm.img
  is_download: true
  type: EXT4_IMG
  linear_start_addr: 0x2d000000
  physical_start_addr: 0x2d000000
  partition_size: 0x10000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS37
  partition_name: vendor
  file_name: vendor.img
  is_download: true
  type: EXT4_IMG
  linear_start_addr: 0x3d000000
  physical_start_addr: 0x3d000000
  partition_size: 0x46000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS38
  partition_name: system
  file_name: system.img
  is_download: true
  type: EXT4_IMG
  linear_start_addr: 0x83000000
  physical_start_addr: 0x83000000
  partition_size: 0x180000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS39
  partition_name: vbmeta
  file_name: vbmeta.img
  is_download: true
  type: NORMAL_ROM
  linear_start_addr: 0x203000000
  physical_start_addr: 0x203000000
  partition_size: 0x800000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: true
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS40
  partition_name: cache
  file_name: cache.img
  is_download: true
  type: EXT4_IMG
  linear_start_addr: 0x203800000
  physical_start_addr: 0x203800000
  partition_size: 0x1b000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS41
  partition_name: userdata
  file_name: userdata.img
  is_download: true
  type: EXT4_IMG
  linear_start_addr: 0x21e800000
  physical_start_addr: 0x21e800000
  partition_size: 0xc0000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: true
  is_reserved: false
  operation_type: UPDATE
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS42
  partition_name: otp
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0xffff01d8
  physical_start_addr: 0xffff01d8
  partition_size: 0x2b00000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: false
  is_reserved: true
  operation_type: RESERVED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS43
  partition_name: flashinfo
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0xffff0080
  physical_start_addr: 0xffff0080
  partition_size: 0x1000000
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: false
  is_reserved: true
  operation_type: RESERVED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

- partition_index: SYS44
  partition_name: sgpt
  file_name: NONE
  is_download: false
  type: NORMAL_ROM
  linear_start_addr: 0xffff0000
  physical_start_addr: 0xffff0000
  partition_size: 0x4200
  region: EMMC_USER
  storage: HW_STORAGE_EMMC
  boundary_check: false
  is_reserved: true
  operation_type: RESERVED
  is_upgradable: false
  empty_boot_needed: false
  reserve: 0x00

